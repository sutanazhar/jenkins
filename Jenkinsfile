pipeline {
  agent any

  tools { 
    maven 'Maven'  
  }

  environment {
    def dateTag = sh(script: 'date "+%d-%m-%Y"', returnStdout: true).trim()
    TEXT_SUCCESS_BUILD = "Pipeline ${env.JOB_NAME} on build number ${BUILD_NUMBER} is Success"
    TEXT_FAILURE_BUILD = "Pipeline ${env.JOB_NAME} on build number ${BUILD_NUMBER} is Failure"
    BUILD_APP_NAME = "maven"
    VERSION = "${dateTag}-${BUILD_NUMBER}"
  }


  stages{
    stage('Sonarqube Scan') {
      steps {
        script {
          withCredentials([string(credentialsId: 'SONARQUBE_TOKEN', variable: 'SONARQUBE_TOKEN')]) {
            sh 'mvn clean verify sonar:sonar -Dsonar.projectKey=${SONARQUBE_PROJECT_MAVEN} -Dsonar.organization=${SONARQUBE_PROJECT_MAVEN} -Dsonar.host.url=${SONARQUBE_HOST} -Dsonar.token=${SONARQUBE_TOKEN}'
          }
        }
      }
    }

    stage('Snyk Scan') {
      steps {
        script {
          withCredentials([string(credentialsId: 'SNYK_TOKEN', variable: 'SNYK_TOKEN')]) {
            sh "ssh ${TOOLS_LOGIN} 'rm -rf ${TOOLS_HOME}${MAVEN_REPOSITORY}'"
            sh "ssh ${TOOLS_LOGIN} 'git clone https://gitlab.com/sutanazhar/${MAVEN_REPOSITORY}.git'"
            sh "ssh ${TOOLS_LOGIN} 'snyk auth ${SNYK_TOKEN}'"
				    sh "ssh ${TOOLS_LOGIN} 'snyk monitor ${TOOLS_HOME}${MAVEN_REPOSITORY} --token=${SNYK_TOKEN}'"
          }
        }
      }
    }

    stage('Build Image') { 
      steps { 
        script{
          app =  docker.build("${HARBOR_URL}/${DEFAULT_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION}")
          }
        }
      }

    stage('Push') {
      steps {
        script{
          docker.withRegistry("http://${HARBOR_URL}", 'harbor') {
            app.push("${VERSION}")
          }
        }
      }
    }

    stage('Run Docker Container') {
      steps {
        script {
          def containerList = sh(script: "ssh ${TOOLS_LOGIN} 'docker ps -aq'", returnStdout: true).trim()
          if (containerList.isEmpty()) {
            echo 'No running Docker container found. Skipping deletion.'
          } else {
            echo 'Stopping and deleting the Docker container...'
            //sh "ssh ${TOOLS_LOGIN} 'docker stop \$(docker ps -aq)'"
            //sh "ssh ${TOOLS_LOGIN} 'docker rm \$(docker ps -aq)'"
          }
          withCredentials([string(credentialsId: 'HARBOR_USER', variable: 'HARBOR_USER'), string(credentialsId: 'HARBOR_PASSWORD', variable: 'HARBOR_PASSWORD')]) {
            def dateTag = sh(script: 'date "+%d-%m-%Y"', returnStdout: true).trim()
            sh "ssh ${TOOLS_LOGIN} 'docker login -u ${HARBOR_USER} -p ${HARBOR_PASSWORD} http://${HARBOR_URL}'"
            sh "ssh ${TOOLS_LOGIN} 'docker run -d --name ${JOB_NAME}_${BUILD_NUMBER} -p 81:8080 ${HARBOR_URL}/${DEFAULT_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION}'"
          }
        }
      }
    }


    stage ('Wait'){
      steps {
        sh 'pwd; sleep 30; echo "Wait for application deployment.."'
      }
    }

	  stage('Owasp ZAP Scan') {
      steps {
				sh "ssh ${ROOT_LOGIN} '/snap/zaproxy/28/zap.sh -cmd -quickurl http://localhost:81 -quickprogress -quickout ${TOOLS_HOME}${JOB_NAME}_zap_${BUILD_NUMBER}.html'"
        sh "ssh ${ROOT_LOGIN} 'scp ${TOOLS_HOME}${JOB_NAME}_zap_${BUILD_NUMBER}.html root@103.30.195.149:${env.WORKSPACE}'"
				archiveArtifacts artifacts: "${JOB_NAME}_zap_${BUILD_NUMBER}.html"
		  }
    } 

    stage('Trivy Scan') {
      steps {
        script {
          withCredentials([string(credentialsId: 'HARBOR_SCAN_AUTH', variable: 'HARBOR_SCAN_AUTH'), string(credentialsId: 'HARBOR_SCAN_TOKEN', variable: 'HARBOR_SCAN_TOKEN')]) {
            def dateTag = sh(script: 'date "+%d-%m-%Y"', returnStdout: true).trim()
            sh "ssh ${ROOT_LOGIN} 'trivy image --format json --output ${JOB_NAME}_trivy_${BUILD_NUMBER}.json ${HARBOR_URL}/${DEFAULT_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION}'"
            sh "ssh ${ROOT_LOGIN} 'scp /root/${JOB_NAME}_trivy_${BUILD_NUMBER}.json root@103.30.195.149:${env.WORKSPACE}'"
            archiveArtifacts artifacts: "${JOB_NAME}_trivy_${BUILD_NUMBER}.json"
            sh "curl --insecure -X 'POST' \
  'http://${HARBOR_URL}/api/v2.0/projects/${DEFAULT_HARBOR_PROJECT}/repositories/${BUILD_APP_NAME}/artifacts/${VERSION}/scan' \
  -H 'accept: application/json' \
  -H 'authorization: Basic ${HARBOR_SCAN_AUTH}' \
  -H 'X-Harbor-CSRF-Token: ${HARBOR_SCAN_TOKEN}' \
  -d ''"
          }
        }
      }
    }

    stage('Push Scanned Image') {
      steps {
        script{
          def dateTag = sh(script: 'date "+%d-%m-%Y"', returnStdout: true).trim()
          sh "ssh ${TOOLS_LOGIN} 'docker tag ${HARBOR_URL}/${DEFAULT_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION} ${HARBOR_URL}/${SCANNED_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION}'"
          sh "ssh ${TOOLS_LOGIN} 'docker push ${HARBOR_URL}/${SCANNED_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION}'"
          sh "ssh ${TOOLS_LOGIN} 'docker tag ${HARBOR_URL}/${DEFAULT_HARBOR_PROJECT}/${BUILD_APP_NAME}:${VERSION} ${HARBOR_URL}/${SCANNED_HARBOR_PROJECT}/${BUILD_APP_NAME}:latest'"
          sh "ssh ${TOOLS_LOGIN} 'docker push ${HARBOR_URL}/${SCANNED_HARBOR_PROJECT}/${BUILD_APP_NAME}:latest'"
        }
      }
    }

    //stage('Clean Unused Image and Container') {
    //  steps {
    //    script{
    //      def dateTag = sh(script: 'date "+%d-%m-%Y"', returnStdout: true).trim()
    //      sh "ssh ${TOOLS_LOGIN} 'docker stop \$(docker ps -aq)'"
    //      sh "ssh ${TOOLS_LOGIN} 'docker rm \$(docker ps -aq)'"
    //      sh "ssh ${TOOLS_LOGIN} 'docker rmi \$(docker images -q) --force'"
    //      sh "docker rmi \$(docker images -q) --force"
    ////      sh "ssh ${SCA_LOGIN} 'rm -rf ${SCA_HOME}test-project'"
    ////    }
    ////  }
    ////}
//
    //stage('Deploy to Kubernetes using Helm') {
	  //  steps {
	  //    script {
    //      def valuesContent = """
    //                    replicaCount: 1
    //                    image:
    //                      repository: ${SCANNED_IMAGE_NAME}
    //                      tag: ${VERSION}
    //                      project: dev-${BUILD_APP_NAME}
    //                      helm: helm-dev-${BUILD_APP_NAME}
    //                """.stripMargin()
//
    //                writeFile file: 'valuesx.yaml', text: valuesContent
//
    //      def playbookContent = """
    //                    - hosts: k8s-master
    //                      become: yes
    //                      tasks:
    //                        - name: copy file from jenkins to kubemaster
    //                          copy:
    //                            src: "{{ item }}"
    //                            dest: /helm/helm-development
    //                          loop:
    //                            - values.yaml 
    //                          notify:
    //                            - upgrade_service
    //                            - install_chart
    //                      handlers:
    //                        - name: install_chart
    //                          shell: helm install dev-${BUILD_APP_NAME} /helm/helm-development
    //                          ignore_errors: True
    //                        - name: upgrade_service
    //                          shell: helm upgrade dev-${BUILD_APP_NAME} /helm/helm-development
    //                """.stripMargin()
//
//
    //                writeFile file: 'playbook.yaml', text: playbookContent
    //      // Run Ansible playbook
    //      sh 'cp valuesx.yaml values.yaml'
    //      sh 'ansible-playbook playbook.yaml'
	  //    }
	  //  }
   	//}
    //
  }

}
